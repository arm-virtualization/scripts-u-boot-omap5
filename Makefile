BUILD_DIR=build

sd-noip:
	mkdir -p $(BUILD_DIR)
	mkimage -A arm -T script -C none -n "Boot Image" -d boot.script.tftp.sd.noip $(BUILD_DIR)/boot.scr.tftp.sd.noip
	cp $(BUILD_DIR)/boot.scr.tftp.sd.noip boot.scr

sd:
	mkdir -p $(BUILD_DIR)
	mkimage -A arm -T script -C none -n "Boot Image" -d boot.script.sd $(BUILD_DIR)/boot.scr.sd
	cp $(BUILD_DIR)/boot.scr.sd boot.scr

tftp-nfs:
	mkdir -p $(BUILD_DIR)
	mkimage -A arm -T script -C none -n "Boot Image" -d boot.script.tftp.nfs $(BUILD_DIR)/boot.scr.tftp.nfs
	cp $(BUILD_DIR)/boot.scr.tftp.nfs boot.scr

tftp-sd:
	mkdir -p ($BUILD_DIR)
	mkimage -A arm -T script -C none -n "Boot Image" -d boot.script.tftp.sd $(BUILD_DIR)/boot.scr.tftp.sd
	cp $(BUILD_DIR)/boot.scr.tftp.sd boot.scr

install-tftp:
	cp boot.scr /tftpboot/boot.scr
	
install-sd:
	sudo cp boot.scr /media/boot/boot.scr
	
	

