Introduction
======

This repository contains the boot scripts created during the arm virtualization project.

They can be used to boot the TI OMAP5432 board with various configurations:

- Boot with kernel + dtb + rootfs from SD Card
- Boot with kernel + dtb from tftp and rootfs from SD Card
- Boot with kernel + dtb from tftp and rootfs from NFS

Prerequisites
======

You need the mkimage and make binary available in your path in order to use these scripts

Usage
======

All is controlled by the provided Makefile.

Following targets are available:

- sd: kernel+dtb+rootfs from sdcard
- sd-noip: kernel+dtb+rootfs from sdcard, but no ip is given in the bootargs (eth0 card will not be configured during kernel boot)
- tftp-nfs: kernel+dtb from tftp, rootfs from nfs
- tftp-sd: kernel+dtb from tftp, rootfs from sdcard
- install-tftp: copy the generated boot script to /tftpboot folder
- install-sd: copy the generated script to a SD Card prepared with TI SDK for OMAP5432 Board


Compiled boot script are created in the build/ subfolder.


IP configuration is set in the boot script file. Modify according to your needs.


Example
======

Generate the tftp-nfs image
	make tftp-nfs

Install it to the sd card
	make install-sd

